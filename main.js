const config = require('./config.js');
const express = require('express');
const db = require('./db.js');
const app = express();
const users = require('./routes/users')
const friends = require('./routes/friends.js')
const msgs = require('./routes/msgs.js')
const convs = require('./routes/convs.js')

//REMOVE USER FLA ON STARTUP!!!
if (!config.jwtPrivateKey){
    console.log('no jwtPrivateKey');
    process.exit(1);
};

app.use(express.json());
app.use('/static', express.static('static'));
app.use('/api/users', users.router)
app.use('/api/friends', friends.router)
app.use('/api/msgs', msgs.router)
app.use('/api/convs', convs.router)

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/html/index.html')
});

db.connect(config.dbPath)
    .then(()=>{ 
        console.log('connected to mongoDB on :', config.dbPath)
        app.listen(config.PORT, ()=>{
            console.log('listening on port:', config.PORT);
        });
    })
    .catch((err)=>{
        console.log(err.message)
        process.exit(1);
    })
