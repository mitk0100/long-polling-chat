const express = require('express');
const router = express.Router();
const db = require('../db.js');
const users = require('./users.js');
const dbCollection = 'testconv';
const mongodb = require('mongodb');

function createConvObj(users, name){
    //use an array of user ids
    validateConvObject(users, name)
    if(users.length < 3){
        name = '-'
    }
    return {
        participants: users,
        name: name,
        lastIndex: 0,
        messages:[]
    }
}
function validateConvObject(users, name){
    if(!(users.constructor === Array)){
        throw('Not an array.')
    };
    if(users.length > 2 && name.length < 4){
        if(!name){
            throw('No name for group conversation.')
        }
        throw('name too short.')
    };
};
function pushConv(convObj){
    return new Promise( async (resolve, reject) => {
        try {
            // FIX STUPID ERROR THROW FOR NO ITEMS FOUND!!!!
            const usersArr = convObj.users;
            const similarObj = await db.read(dbCollection, {users: usersArr});
            if(similarObj){
                //console.log('Simillar conversation obj found.');
                //console.log(similarObj);
                throw('Conversation already present. Nothing new was made.');
            };
            resolve(dbObj);
        } catch (err) {
            if(err = 'No items found.'){
                const dbObj = await db.create(dbCollection, convObj);
                resolve(dbObj);
            };
            reject(err);
        };
    });
};
router.get('/meta', async (req, res)=>{
    try{
        const jwtLoad = users.auth(req);
        //console.log(mongodb.ObjectID.createFromHexString(jwtLoad._id))
        const convs = await db.read(dbCollection, {participants: {id: mongodb.ObjectID.createFromHexString(jwtLoad._id) ,name:jwtLoad.name} }, {"messages": 0}, true);
        res.send(convs)
    } catch (err) {
        console.log(err || err.message)
        res.status(404).send('resource not found')
    }
})

module.exports.createConvObj = createConvObj; //built in validation
module.exports.pushConv = pushConv; //built in validation
module.exports.router = router;