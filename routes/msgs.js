const express = require('express');
const router = express.Router();
const db = require('../db.js');
const users = require('./users.js');
const convs = require('./convs.js');
const dbCollection = 'testconv';
const EventEmitter = require('events');
const msgEventEmitter = new EventEmitter();
const mongodb = require('mongodb');
//&lt
//last index
function createMsgObj(req, id){
    if(!req.body.msg || req.body.msg.length < 1){
        throw('no message');
    } else if(req.body.msg.length > 500){
        throw('message too long');
    };
    const msg = req.body.msg.replace(/</g, '&lt;').replace(/\(/g, '&#40;').replace(/,/g, '&#44;').replace(/\//g, '&#47;').replace(/'/g, '&#39;').replace(/"/g, '&#34;');
    const recT = Date.now();
    return {
        sender: id,
        msg: msg,
        recT: recT,
        e: false, //stands for edited
        d: false //stands for deleted
    };
};
router.get('/byid/:convid', async (req, res) => {
    try{
        const convid = req.params.convid
        if(!convid){
            return res.status(400).send('Bad request.');
        };
        const jwtLoad = users.auth(req);
        const convObj = await db.read(dbCollection, {_id: convid});
        const foundUser = convObj.participants.find((obj)=>{
            return obj.name == jwtLoad.name;
        });
        console.log(foundUser);
        if(!foundUser) throw('Unauthurized');
        res.send(convObj.messages);
    } catch(err) {
        res.status(404).send('conv not found.')
    };
});
router.post('/byid/:convid', async (req, res) => {
    try{
        const convid = req.params.convid
        if(!convid){
            return res.status(400).send('Bad request.')
        };
        const jwtLoad = users.auth(req);
        const msgObj = createMsgObj(req, jwtLoad.name);
        const convObj = await db.read(dbCollection, {_id: convid});
        var otherUserName;
        await new Promise(async (resolve, reject) => {
            try{
                for(x = 0; x < convObj.participants.length; x++){
                    convObj.participants[x] = await db.read(users.dbCollection, {_id: convObj.participants[x].id});
                    convObj.participants[x].id = convObj.participants[x]._id;
                    if(convObj.participants[x].name != jwtLoad.name){
                        otherUserName = convObj.participants[x].name;
                    }
                }
                resolve()
            } catch(err) {
                res.status(400).send('incorect info.')
            }
        })
        const blockStat = users.testBlock(convObj.participants[0], convObj.participants[1]);
        if(blockStat == 1 || blockStat == 3){
            return res.status(403).send('User blocked. No msg was sent.')
        } else if (blockStat == 2) {
            return res.status(403).send('You are blocked by this user.')
        };
        const frStat = users.testFriends(convObj.participants[0], convObj.participants[1]);
        if(frStat == 0){
            return res.status(403).send('You are not a friend of this user.')
        }
        await db.update(dbCollection, convObj._id, {$push: {messages: msgObj}});
        await db.update(dbCollection, convObj._id, {$inc: {lastIndex: 1}});
        msgEventEmitter.emit('newmsg', {to: otherUserName, convid: convid, msgObj: msgObj, convLastIndex: convObj.lastIndex + 1});
        res.send({recT: msgObj.recT});
    } catch(err) {
        res.status(400).send('bad request');
    };
});
router.put('/byid/:convid', async (req, res) => {
    try{
        throw('not implemented');
        const indNum = req.body.indNum;
        const convid = req.params.convid;
        if(!convid || !indNum){
            return res.status(400).send('Bad request.')
        };
    } catch(err) {
        //res.status(400).send('bad request');
        res.status(404).send('not implimented');
    };
});
router.delete('/byid/:convid', async (req, res) => {
    try{
        throw('not implemented');
        const indNum = req.body.indNum;
        const convid = req.params.convid;
        if(!convid || !indNum){
            return res.status(400).send('Bad request.')
        };
    } catch(err) {
        //res.status(400).send('bad request');
        res.status(404).send('not implimented');
    };
});
router.get('/fromindex', async (req, res)=>{
    try{
        const jwtLoad = users.auth(req);
        const convid = req.header('convid');
        if(!convid) throw ({message: 'no convid'});
        const fromIndex = req.header('fromindex');
        if(!fromIndex) throw ({message: 'no fromindex'});
        const convDb = await db.read(dbCollection, {
            _id:  mongodb.ObjectID.createFromHexString(convid), 
            participants: {id: mongodb.ObjectID.createFromHexString(jwtLoad._id) ,name:jwtLoad.name} 
        }, {"messages": 1, "lastIndex": 1});
        const msgNum = convDb.lastIndex - fromIndex
        if(msgNum < 1 || !msgNum) throw ({message: 'msgNum < 1'});
        convDb.messages = convDb.messages.slice(-msgNum)
        res.status(200).send(convDb);
    } catch(err) {
        console.log(err.message);
        res.status(500).send('Oops, something went wrong')
    }

})

router.get('/longpoll/:uname', async (req, res)=>{
    try{
        const jwtLoad = users.auth(req);
        if(jwtLoad.name != req.params.uname) res.status(404).send()
        const poll = await new Promise( async (resolve, reject)=>{
            try{
                //check if new message was sent inbetween reqs
                    const convArr = JSON.parse(req.header('conv-arr'));
                    const convsDb = await db.read(dbCollection, {participants: {id: mongodb.ObjectID.createFromHexString(jwtLoad._id) ,name:jwtLoad.name} }, {"messages": 0}, true);
                    convArr.forEach(async (arrObj)=>{
                        var dbMatch = convsDb.find((dbObj)=>{
                            return mongodb.ObjectID(dbObj._id).valueOf() == arrObj.convid
                        })
                        if(dbMatch.lastIndex > Number(arrObj.lastIndex)){
                            //send a response instructing the browser to request an update of the conversation for indexes convArr till the end for any msgs that aren't sent by the requestor
                            resolve({updateByIndex:true, convid: arrObj.convid, fromIndex: arrObj.lastIndex, lastIndex: dbMatch.lastIndex})
                        }
                    })
                //poll starts here
                setTimeout(()=>{
                    resolve(false)
                }, 30000);
                msgEventEmitter.prependListener('newmsg', (dataObj)=>{
                    if(dataObj.to == jwtLoad.name){
                        resolve(dataObj);
                    }
                })
            } catch(err){
                console.log(err.message || err);
                return res.status(500).send('Oops, something went wrong :/');
            }
        });
        if(!poll){
            return res.status(204).send({info: 'Nothing new.'});
        }
        return res.status(200).send(poll);
    } catch (err) {
        console.log(err.message || err);
        res.status(500).send('Oops, something went wrong :/');
    }
})

/*
router.get('/longpoll', async (req, res)=>{
    try{
        req.setTimeout(0)
        const jwtLoad = users.auth(req);
        //check if new message was sent inbetween reqs
        const convArr = JSON.parse(req.header('conv-arr'));
        const convsDb = await db.read(dbCollection, {participants: {id: mongodb.ObjectID.createFromHexString(jwtLoad._id) ,name:jwtLoad.name} }, {"messages": 0}, true);
        convArr.forEach(async (arrObj)=>{
            var dbMatch = convsDb.find((dbObj)=>{
                return mongodb.ObjectID(dbObj._id).valueOf() == arrObj.convid
            })
            if(dbMatch.lastIndex > Number(arrObj.lastIndex)){
                //send a response instructing the browser to request an update of the conversation for indexes convArr till the end for any msgs that aren't sent by the requestor
                return res.status(200).send({updateByIndex:true, convid: arrObj.convid, fromIndex: arrObj.lastIndex, lastIndex: dbMatch.lastIndex})
            }
        })
        //poll starts here
        setTimeout(()=>{
            return res.status(204).send({info: 'Nothing new.'});
        }, 30000);
        msgEventEmitter.prependListener('newmsg', (dataObj)=>{
            if(dataObj.to == jwtLoad.name){
                res.status(200).send(dataObj);
            }
        })
    } catch (err) {
        console.log(err.message || err);
        res.status(500).send('Oops, something went wrong :/');
    }
})
*/
module.exports.router = router;
module.exports.createMsgObj = createMsgObj; //built in user input validation