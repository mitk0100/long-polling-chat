const express = require('express');
const config= require('../config.js')
const bcrypt = require('bcrypt');
const db = require('../db.js');
const jwt = require('jsonwebtoken')
const router = express.Router();
const dbCollection = 'test'
const mongodb = require('mongodb');

function createUserObject(req){
    try {
        const salt = bcrypt.genSaltSync(10);
        return {
            name: req.body.name,
            password: bcrypt.hashSync(req.body.password, salt),
            gender: req.body.gender,
            country: req.body.country,
            friends:[],
            req:[],
            blocked:[],
            status: false,
            fla: 0 //failed log in attempts
        };
    } catch(err) {
        throw(err.message);
    };
};
function validateReqData(req){
    const name = req.body.name;
    const password = req.body.password;
    const gender = req.body.gender;
    const country = req.body.country;
    if(!name || !password || !gender || !country){
        throw('missing parameter');
    } else if (/^\d/.test(name) || /\W+/.test(name)){
        throw('name must start with a letter and contain only letters and numbers - ' + name);
    } else if (name.length > 20 || password.length < 8 || password.length > 500){
        throw('either name or password is too short or too long');
    } else if (/\W+/.test(country)){
        throw('country should contain only letters');
    } else if (country.length > 20){
        throw('country name too long');
    } else if (gender != 'male' && gender != 'female'){
        throw('gender can be only male or female');
    };
    return true;
};
function uniqueName(name){
    return new Promise( async (resolve, reject)=> {
        try {
            let res = await db.read(dbCollection, {name: name})
            if(res) resolve(false)
        } catch(err) {
            if(err == 'No items found.') {
                resolve(true)
            } else {
                reject(err)
            }
        }
    })
}
function genToken(dbObj){
    obj = {
        name: dbObj.name,
        _id: dbObj._id,
        gender: dbObj.gender,
        country: dbObj.country
    }
    return jwt.sign(obj, config.jwtPrivateKey)
}
function verifyToken(token){
    const jwtObj = jwt.verify(token, config.jwtPrivateKey);
    return jwtObj;
}
function correctPassword(reqPassword, dbPassword){
    return bcrypt.compareSync(reqPassword, dbPassword)
}
//routers start here
router.get('/availablename/:name', async (req, res)=>{
    try{
        const srchName = req.params.name;
        await db.read(dbCollection, {name: srchName})
        res.status(200).send({available: false})
    } catch (err) {
        res.status(200).send({available: true})
    }
})
router.get('/me', async(req, res)=>{
    try{
        const jwtLoad = auth(req);
        const userObj = await db.read(dbCollection, {_id: mongodb.ObjectID.createFromHexString(jwtLoad._id)}, {name:1, gender:1, country: 1});
        res.status(200).send(userObj);
    } catch(err) {
        res.status(401).send('unauthenticated');
    }
})
router.post('/new', async (req, res)=>{
    try{
        validateReqData(req)
        const unique = await uniqueName(req.body.name)
        if(!unique) throw ('name already taken')
        const userObj = createUserObject(req)
        const dbObj = await db.create(dbCollection ,userObj)
        const token = genToken(dbObj)
        res.header('x-api-key', token).send()
    } catch (err) {
        res.status(400).send(err)
    }
})
router.post('/login', async (req, res)=>{
    try{
        const dbObj = await db.read(dbCollection,{name: req.body.name})
        if(dbObj.fla > 3){
            //limit of 5 reached
            return res.status(403).send('4 failed login attempts within 10 minutes');
        };
        if(!correctPassword(req.body.password, dbObj.password)){
            await db.update(dbCollection, dbObj._id, {$set: {fla: dbObj.fla + 1}});
            if (dbObj.fla <= 3) {
                setTimeout(async ()=>{
                    await db.update(dbCollection, dbObj._id, {$set: {fla: 0}});
                },60000);
            }
            //return res.status(400).send('failed login attempts: ' + dbObj.fla);
            throw('Wrong password')
        }
        const token = genToken(dbObj);
        res.header('x-api-key', token).send();
    } catch (err){
        res.status(400).send('Wrong username or password.');
    }
})
router.post('/auth', async (req, res) => {
    try{
        const jwtLoad = auth(req);
        res.send({name: jwtLoad.name})
    } catch (err) {
        res.status(401).send('Authintication failed')
    }
})
//authentication
function auth(req){
    authToken = req.header('x-api-key');
    if(!authToken) throw('no api key');
    const jwtObj = verifyToken(authToken);
    return jwtObj;
}
function testBlock(user1, user2){
    //returns a 1, 2 or 3 depending on which user has blocked who, otherwise returns false
    let res = 0
    user1.strid = db.mongodb.ObjectID(user1._id).toString()
    user2.strid = db.mongodb.ObjectID(user2._id).toString()
    if(user1.blocked){
        if(JSON.stringify(user1.blocked.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user2.strid, name: user2.name})})) === JSON.stringify({id: user2.strid, name: user2.name}))
        {
            res += 1
        }
    }
    if(user2.blocked){
        if(JSON.stringify(user2.blocked.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user1.strid, name: user1.name})})) === JSON.stringify({id: user1.strid, name: user1.name}))
        {
            res += 2
        }
    }
    if (res == 0) {
        return false
    } else {
        return res
    }
}
function testReq(user1, user2){
    //returns a 1, 2 or 3 depending on which user has requested to who, otherwise returns false
    let res = 0
    user1.strid = db.mongodb.ObjectID(user1._id).toString()
    user2.strid = db.mongodb.ObjectID(user2._id).toString()
    //console.log(user1.req)
    //console.log(user2.req)
    //
    /*user2.req.find((id)=>{
        console.log(`${db.mongodb.ObjectID.ObjectID(id).toString()} - ${user2.strid}`)
        return db.mongodb.ObjectID.ObjectID(id).toString() == user1.strid
    })*/
    //
    if(user1.req){
        if(JSON.stringify(user1.req.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user2.strid, name: user2.name})})) === JSON.stringify({id: user2.strid, name: user2.name}))
        {
            res += 1
        }
    }
    if(user2.req){
        if(JSON.stringify(user2.req.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user1.strid, name: user1.name})})) === JSON.stringify({id: user1.strid, name: user1.name}))
        {
            res += 2
        }
    }
    if (res == 0) {
        return false
    } else {
        return res
    }
}
function testFriends(user1, user2){
    //returns a 1, 2 or 3 depending on which user has befriended who, otherwise returns false
    let res = 0
    user1.strid = db.mongodb.ObjectID(user1._id).toString()
    user2.strid = db.mongodb.ObjectID(user2._id).toString()
    if(user1.friends){
        if(JSON.stringify(user1.friends.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user2.strid, name: user2.name})})) === JSON.stringify({id: user2.strid, name: user2.name}))
        {
            res += 1
        }
    }
    if(user2.friends){
        if(JSON.stringify(user2.friends.find((obj)=>{
            obj.id = db.mongodb.ObjectID(obj.id).toString()
            return JSON.stringify(obj) === JSON.stringify({id: user1.strid, name: user1.name})})) === JSON.stringify({id: user1.strid, name: user1.name}))
        {
            res += 2
        }
    }
    if (res == 0) {
        return false
    } else {
        return res
    }
}
function compareById(user1ID, user2ID){
    //compares two users by their ids, if they are the same, returns true
    let id1 = db.mongodb.ObjectID(user1ID).toString()
    let id2 = db.mongodb.ObjectID(user2ID).toString()
    if (id1 == id2){
        return true
    } else {
        return false
    }
}
//test
/*
router.post('/authtest', async (req, res)=>{
    try{
        const varx = auth(req)
        res.send(varx)
    } catch (err){
        res.status(400).send({errObj: err, msg: err.message})
    }
})
*/
module.exports.createUserObject = createUserObject;
module.exports.validateReqData = validateReqData;
module.exports.uniqueName = uniqueName;
module.exports.genToken = genToken;
module.exports.correctPassword = correctPassword;
module.exports.verifyToken = verifyToken;
module.exports.auth = auth;
module.exports.testBlock = testBlock;
module.exports.compareById = compareById;
module.exports.testReq = testReq;
module.exports.testFriends = testFriends;
module.exports.router = router;
module.exports.dbCollection = dbCollection;