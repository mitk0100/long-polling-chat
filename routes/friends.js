const express = require('express');
const router = express.Router();
const db = require('../db.js');
const users = require('./users.js');
const convs = require('./convs.js')
const dbCollection = 'test';
/*BOILERPLATE
try {

} catch (err) {
    if (err == "'No items found.'"){
        return res.status(400).send('User not found.')
    } else {
        return res.status(403).send('Access denied.')
    }
}
*/

//G E T   I N F O
router.get('/', async (req, res) => {
    try {
        const jwtObj = users.auth(req)
        const dbObj = await db.read(dbCollection, {_id: jwtObj._id})
        res.send({friends: dbObj.friends, req: dbObj.req, blocked: dbObj.blocked});
    } catch(err){
        res.status(403).send('Access denied.')
    }
})
router.get('/:op', async (req, res) => {
    try {
        const op = req.params.op
        if(op != 'fr' && op != 'req' && op != 'blocked' && op != 'inreq'){
            res.status(400).send('Bad request.')
        }
        const jwtObj = users.auth(req)
        const dbObj = await db.read(dbCollection, {_id: jwtObj._id})
        switch (op) {
            case 'fr':
                res.send(dbObj.friends);
                break;
            case 'req':
                res.send(dbObj.req)
                break;
            case 'blocked':
                res.send(dbObj.blocked)
                break;
            case 'inreq':
                const usersArr = await db.read(dbCollection, {req: {id:dbObj._id, name:dbObj.name } }, {name:1}, true);
                res.status(200).send(usersArr);
                break;
        }
    } catch(err){
        res.status(403).send('Access denied.');
        console.log(err || err.message)
    }
})

//R E Q U E S T
router.post('/req/send/:id', async (req, res) =>{
    try {
        const id = req.params.id
        if(!id){
            return res.status(400).send('Bad request.')
        }
        const jwtLoad = users.auth(req)
        const jwtObj = await db.read(dbCollection, {_id: jwtLoad._id})
        const recObj = await db.read(dbCollection, {name: id})
        if (users.compareById(jwtObj._id, recObj._id)){
            return res.status(403).send("you can be your best friend if you want, but you can't send a friend request to yourself.")
        }
        const blockStat = users.testBlock(jwtObj, recObj)
        if(blockStat == 1 || blockStat == 3){
            return res.status(403).send('User blocked. No friend request was sent.')
        } else if (blockStat == 2) {
            return res.status(403).send('You are blocked by this user.')
        }
        const reqStat = users.testReq(jwtObj, recObj)
        if(reqStat == 2 || reqStat == 3){
            return res.status(403).send('A request was already sent to this user.')
        } else if (reqStat == 1){
            return res.status(403).send('You already have a friend request pending by this user.')
        }
        const frStat = users.testFriends(jwtObj, recObj)
        if(frStat == 3){
            return res.status(403).send('You are already friends with this user.')
        }
        await db.update(dbCollection, recObj._id, {$push: {req: {id: jwtObj._id, name: jwtObj.name}}})
        return res.send(`Friend request successfully sent to ${recObj.name}.`)
    } catch (err) {
        if (err == "'No items found.'"){
            return res.status(404).send('User not found.')
        } else {
            return res.status(403).send('Access denied.')
        }
    }
})
router.post('/req/accept/:id', async (req, res) => {
    try {
        const id = req.params.id
        if(!id){
            return res.status(400).send('Bad request.')
        }
        const jwtLoad = users.auth(req)
        const jwtObj = await db.read(dbCollection, {_id: jwtLoad._id})
        const recObj = await db.read(dbCollection, {_id: id})
        if (users.compareById(jwtObj._id, recObj._id)){
            return res.status(403).send("can't accept a friend request from yourself.")
        }
        const blockStat = users.testBlock(jwtObj, recObj)
        if(blockStat == 1 || blockStat == 3){
            return res.status(403).send('User blocked. No friend request was accepted.')
        } else if (blockStat == 2) {
            return res.status(403).send('You are blocked by this user.')
        }
        const reqStat = users.testReq(jwtObj, recObj)
        if(reqStat == 2){
            return res.status(400).send('Bad request.')
        } else if (!reqStat){
            return res.status(404).send('No request is pending.')
        }
        const frStat = users.testFriends(jwtObj, recObj)
        if(frStat == 3){
            return res.status(403).send('You are already friends with this user. Cancel your request.')
        }
        const convObj = convs.createConvObj([{id: recObj._id, name: recObj.name}, {id: jwtObj._id, name: jwtObj.name}]);
        await db.update(dbCollection, jwtObj._id, {$pull: {req: {id: recObj._id, name: recObj.name}}});
        await db.update(dbCollection, jwtObj._id, {$push: {friends: {id: recObj._id, name: recObj.name}}});
        await db.update(dbCollection, recObj._id, {$push: {friends: {id: jwtObj._id, name: jwtObj.name}}});
        await convs.pushConv(convObj);
        return res.send(`Successfully befriended ${recObj.name}.`);
    } catch (err) {
        if (err == "'No items found.'"){
            return res.status(404).send('User not found.')
        } else {
            return res.status(403).send(err.message)
        }
    }
})
router.post('/req/reject/:id', async (req, res) => {
    try {
        const id = req.params.id
        if(!id){
            return res.status(400).send('Bad request.')
        }
        const jwtLoad = users.auth(req)
        const jwtObj = await db.read(dbCollection, {_id: jwtLoad._id})
        const recObj = await db.read(dbCollection, {_id: id})
        if (users.compareById(jwtObj._id, recObj._id)){
            return res.status(403).send("can't reject a friend request from yourself.")
        }
        const reqStat = users.testReq(jwtObj, recObj)
        if(reqStat == 2){
            return res.status(400).send('Bad request.')
        } else if (!reqStat){
            return res.status(404).send('No request is pending.')
        }
        await db.update(dbCollection, jwtObj._id, {$pull: {req: {id: recObj._id, name: recObj.name}}})
        return res.send(`Rejected friend request from ${recObj.name}.`)
    } catch (err) {
        if (err == "'No items found.'"){
            return res.status(404).send('User not found.')
        } else {
            return res.status(403).send('Access denied.')
        }
    }
})

//B L O C K - UPDATED
router.post('/block/:id', async (req, res)=>{
    try {
        const id = req.params.id
        if(!id){
            return res.status(400).send('Bad request.')
        }
        const jwtLoad = users.auth(req)
        const jwtObj = await db.read(dbCollection, {_id: jwtLoad._id})
        const recObj = await db.read(dbCollection, {_id: id})
        if (users.compareById(jwtObj._id, recObj._id)){
            return res.status(403).send("oi chum, you can't block yourself")
        }
        const blockStat = users.testBlock(jwtObj, recObj)
        if(blockStat == 1 || blockStat == 3){
            return res.status(403).send('User already blocked.')
        }
        await db.update(dbCollection, jwtObj._id, {$pull: {req: {id: recObj._id, name: recObj.name}}})
        await db.update(dbCollection, jwtObj._id, {$pull: {friends: {id: recObj._id, name: recObj.name}}})
        await db.update(dbCollection, recObj._id, {$pull: {req: {id: jwtObj._id, name: jwtObj.name}}})
        await db.update(dbCollection, recObj._id, {$pull: {friends: {id: jwtObj._id, name: jwtObj.name}}})
        await db.update(dbCollection, jwtObj._id, {$push: {blocked: {id: recObj._id, name: recObj.name}}})
        return res.send(`User ${recObj.name}, id:${recObj._id} successfully blocked.`)
    } catch (err) {
        if (err == "'No items found.'"){
            return res.status(404).send('User not found.')
        } else {
            return res.status(403).send('Access denied.')
        }
    }
})
router.post('/unblock/:id', async (req, res)=>{
    try {
        const id = req.params.id
        if(!id){
            return res.status(400).send('Bad request.')
        }
        const jwtLoad = users.auth(req)
        const jwtObj = await db.read(dbCollection, {_id: jwtLoad._id})
        const recObj = await db.read(dbCollection, {_id: id})
        if (users.compareById(jwtObj._id, recObj._id)){
            return res.status(403).send("check yourself before you unblock yourself")
        }
        const blockStat = users.testBlock(jwtObj, recObj)
        if(!blockStat || blockStat == 2){
            return res.status(403).send('User not blocked.')
        }
        await db.update(dbCollection, jwtObj._id, {$pull: {blocked: {id: recObj._id, name: recObj.name}}})
        return res.send(`User ${recObj.name}, id:${recObj._id} successfully unblocked.`)
    } catch (err) {
        if (err == "'No items found.'"){
            return res.status(404).send('User not found.')
        } else {
            return res.status(403).send('Access denied.')
        }
    }
})  
module.exports.router = router;