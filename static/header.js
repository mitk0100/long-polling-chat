//authenticate
//var auth = false
const meta = document.getElementsByTagName('meta')[0]
const nav = new Element('div', null, ['class', 'nav-wrapper light-blue darken-1 header']); //light-blue darken-1 header

const logo = new Element('img', null, ['src', '/static/media/logo.png', 'class', 'brand-logo logo']);
nav.node.appendChild(logo.node);

const list = new Element('ul', null, ['id', 'nav-mobile' ,'class', 'left']);
nav.node.appendChild(list.node);
root.appendChild(nav.node);

root.appendChild(new Element('div', null, ['class', 'body-div']).node)

//setNavState(auth)

function setNavState(auth, name){
    //true for authenticated, false for non
    list.node.innerHTML = ''
    if(auth){
        var items = [
            new Element('li', null, ['onclick', 'toggleUserMenu()', 'class', 'waves-effect waves-light fade-in']),
            //new Element('li', null, ['onclick', '', 'class', 'waves-effect waves-light fade-in']),
            new Element('li', null, ['onclick', 'showChat()', 'class', 'waves-effect waves-light fade-in'])
        ]
        var contents = [
            new Element('i', 'person', ['class', 'material-icons']),
            //new Element('i', 'more_horiz', ['class', 'material-icons']),
            new Element('i', 'chat', ['class', 'material-icons'])
        ]
        items[0].node.appendChild(new Element('div', name, ['class', 'hide-on-mobile']).node)
        for(x = 0; x < items.length; x++){
            items[x].node.appendChild(contents[x].node)
        }
        meta.setAttribute('data-username', name)
    } else {
        var items = [
            new Element('li', null, ['onclick', 'toggleLogInMenu("signup")', 'class', 'waves-effect waves-light fade-in']),
            new Element('li', null, ['onclick', 'toggleLogInMenu("login")', 'class', 'waves-effect waves-light fade-in'])
        ]
        var contents = [
            new Element('i', 'person_outline', ['class', 'material-icons']),
            new Element('i', 'lock', ['class', 'material-icons'])
        ]
        items[0].node.appendChild(contents[0].node)
        items[0].node.appendChild(new Element('div', 'Sign Up').node)
        items[1].node.appendChild(contents[1].node)
        items[1].node.appendChild(new Element('div', 'Log in').node)
    }
    items.forEach((obj) => {
        list.node.appendChild(obj.node)
    })
}
async function showChat(){
    const body = document.querySelector('.body-div');
    const chatDiv = document.querySelector('.chat-div');
    const viewChatDiv = document.querySelector('.view-chat-div');
    if(!chatDiv && !viewChatDiv){
        body.innerHTML = ''
        friendsArr = await getFriends()
        convArr = await getConvs()
        setBodyState(true, friendsArr, convArr)
    }
}
function toggleUserMenu(){
    var userMenuContainer = document.getElementById('user-menu-container')
    if(!userMenuContainer){
        root.appendChild(new Element('div', null, ['id', 'user-menu-container', 'class', 'fade-in z-depth-2'],[
            new Element('div', 'Profile', ['onclick', 'navEditProfile()']).node,
            new Element('div', 'Chat Requests', ['onclick', 'navFriendRequests()']).node,
            new Element('div', 'Active Chats').node,
            new Element('div', 'Blocked Users').node,
            new Element('div', 'Log Out', ['onclick', 'logOut()']).node
        ]).node)
    } else {
        userMenuContainer.setAttribute('class', userMenuContainer.getAttribute('class') + ' fade-out')
        setTimeout(()=>{
            userMenuContainer.outerHTML = ''
        }, 490)
    }
}
async function logOut(){
    //localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
    toggleUserMenu();
    const body = document.querySelector('.body-div');
    const chatDiv = document.querySelector('.chat-div');
    const viewChatDiv = document.querySelector('.view-chat-div');
    if (localStorage.getItem('x-api-key')) localStorage.removeItem('x-api-key');
    if (sessionStorage.getItem('x-api-key')) sessionStorage.removeItem('x-api-key');
    meta.removeAttribute('data-username');
    if (chatDiv) chatDiv.setAttribute('class', chatDiv.getAttribute('class') + ' fade-out');
    if (viewChatDiv) viewChatDiv.setAttribute('class', viewChatDiv .getAttribute('class') + ' fade-out');
    await new Promise((resolve, reject)=>{
        try{
            setTimeout(()=>{
                body.innerHTML = ''
                setNavState(false);
                resolve()
            }, 500)
        } catch (err) {
            console.log(err)
            reject(err.message)
        }
    });
}
async function navEditProfile(){
    //UNFINISHED it's not an important action, so I'll maybe add it in a later version -> 'nothing for now'
    toggleUserMenu();
    if(document.getElementById('menu-edit-profile')) return;
    const body = document.querySelector('.body-div');
    const chatDiv = document.querySelector('.chat-div');
    const viewChatDiv = document.querySelector('.view-chat-div');
    if (chatDiv) chatDiv.setAttribute('class', chatDiv.getAttribute('class') + ' fade-out');
    if (viewChatDiv) viewChatDiv.setAttribute('class', viewChatDiv .getAttribute('class') + ' fade-out');
    usrData = await getMyUserData();
    await new Promise((resolve, reject)=>{
        try{
            setTimeout(()=>{
                body.innerHTML = ''
                resolve()
            }, 500)
        } catch (err) {
            console.log(err)
            reject(err.message)
        }
    });
    body.appendChild(
        
        new Element('div', null, ['id', 'menu-edit-profile', 'class', 'menu-generic fade-in'],[
            new Element('h1', 'Profile').node,
            new Element('div', null, [], [
                new Element('p', `ID: ${usrData._id}`, ['style', 'padding-left:10px; font-size:20px; font-weight:bold; color:#999;']).node,
                new Element('hr').node,
                new Element('span', 'Country:', ['style', 'padding:10px; font-size:20px; font-weight:bold;']).node,
                new Element('input', null, ['style', 'width:300px; height:30px; border:0px; font-size:20px; border-bottom:2px solid gray;'], null, (objNode)=>{
                    objNode.value = usrData.country
                }).node,
                new Element('hr').node,
                new Element('span', 'Gender:', ['style', 'padding:10px; font-size:20px; font-weight:bold;']).node,
                new Element('input', null, ['style', 'width:300px; height:30px; border:0px; font-size:20px; border-bottom:2px solid gray;'], null, (objNode)=>{
                    objNode.value = usrData.gender
                }).node,
                new Element('hr').node
            ]).node
        ]).node
    )
}
async function navFriendRequests(){
    toggleUserMenu();
    if(document.getElementById('menu-friend-requests')) return;
    const body = document.querySelector('.body-div');
    const chatDiv = document.querySelector('.chat-div');
    const viewChatDiv = document.querySelector('.view-chat-div');
    if (chatDiv) chatDiv.setAttribute('class', chatDiv.getAttribute('class') + ' fade-out');
    if (viewChatDiv) viewChatDiv.setAttribute('class', viewChatDiv .getAttribute('class') + ' fade-out');
    usrData = await getMyUserData();
    await new Promise((resolve, reject)=>{
        try{
            setTimeout(()=>{
                body.innerHTML = ''
                resolve()
            }, 500)
        } catch (err) {
            console.log(err)
            reject(err.message)
        }
    });
    body.appendChild(
        new Element('div', null, ['id', 'menu-friend-requests', 'class', 'menu-generic fade-in'],[
            new Element('h1', 'Chat Requests').node,
            new Element('div', null, ['style', 'position:absolute; top:0; height:100%; width:100%; padding-top:125px;'],[
                new Element('div', null, ['id', 'chatreq-sent-main', 'style', 'width:50%; float:left; height:100%; background-color:white; overflow-x:scroll;', 'class', 'z-depth-2'],[
                    new Element('h5', 'Sent', ['class', 'light-blue darken-1']).node,
                    new Element('div', null, ['class', 'req-send-text'], [
                        new Element('textarea', null, ['placeholder', 'Enter username.']).node,
                        new Element('i', 'person_add', ['class', 'material-icons', 'onclick', 'chatReqSend()']).node
                    ]).node
                ], async (node)=>{
                    try{
                        const sentReq = await getFetchReq('/api/friends/inreq');
                        sentReq.forEach((obj)=>{
                            node.appendChild(new Element('div', null, ['class', 'chatreq-item-div'],[
                                new Element('span', obj.name).node
                            ]).node)
                        })
                    }catch(err){
                        node.appendChild(new Element('h5', 'None', ['class', 'chatreq-none']).node);
                    }
                }).node,
                new Element('div', null, ['id', 'chatreq-received-main', 'style', 'width:50%; float:right; height:100%; text-aling:center; background-color:white; overflow-x:scroll;', 'class', 'z-depth-2'],[
                    new Element('h5', 'Received', ['class', 'light-blue darken-1']).node
                ], async (node)=>{
                    try{
                        const recReq = await getFetchReq('/api/friends/req');
                        if (recReq.length < 1) throw('response 0')
                        recReq.forEach((obj)=>{
                            node.appendChild(new Element('div', null, ['class', 'chatreq-item-div', 'data-userid', obj.id],[
                                new Element('span', obj.name).node,
                                new Element('i', 'close', ['class', 'material-icons hover-close', 'onclick', 'chatReqReject(this)']).node,
                                new Element('i', 'check', ['class', 'material-icons hover-check', 'onclick', 'chatReqAccept(this)']).node
                            ]).node)
                        })
                    }catch(err){
                        node.appendChild(new Element('h5', 'None', ['class', 'chatreq-none']).node);
                    }
                }).node
            ]).node
        ]).node
    )
}
async function chatReqSend(){
    const mainSent = document.querySelector('#chatreq-sent-main');
    try{
        const toName = document.querySelector('.req-send-text > textarea').value;
        res = await new Promise(async (resolve, reject) => {
            const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
            try{
                const res = await fetch(`/api/friends/req/send/${toName}`, {
                    method: 'post',
                    headers:{
                        'x-api-key' : apiKey
                    }
                })
                if(res.status == 200){
                    let resObj = await res
                    //CALL OTHER GUI CHANGES
                    resolve(resObj)
                    return
                }
                //alert(ele('warn'))
                reject(false)
            } catch (err) {
                //console.log(err)
                reject(err)
            }
        })
        console.log(res);
        if(document.querySelector('#chatreq-sent-main > .chatreq-none')){
            document.querySelector('#chatreq-sent-main > .chatreq-none').outerHTML = '';
        }
        mainSent.appendChild(new Element('div', null, ['class', 'chatreq-item-div'],[
            new Element('span', toName).node
        ]).node)
        document.querySelector('.req-send-text > textarea').value = '';
    } catch(err) {
        if(!document.getElementById('chatreq-err')){
            mainSent.appendChild(new Element('p', 'Something went wrong.', ['id', 'chatreq-err', 'class', 'fade-in']).node);
            setTimeout(() => {
                document.getElementById('chatreq-err').setAttribute('class', 'fade-out');
                setTimeout(() => {
                    document.getElementById('chatreq-err').outerHTML = '';
                }, 500)
            }, 5000);
        }
    }
}
async function chatReqAccept(obj){
    const objParent = obj.parentNode;
    const byId = objParent.getAttribute('data-userid');
    const byName = objParent.getElementsByTagName('span')[0].innerText;
    const mainRec = document.getElementById('chatreq-received-main');
    //console.log(objParent);
    try{
        await new Promise(async (resolve, reject) => {
            const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
            try{
                const res = await fetch(`/api/friends/req/accept/${byId}`, {
                    method: 'post',
                    headers:{
                        'x-api-key' : apiKey
                    }
                })
                if(res.status == 200){
                    let resObj = await res
                    //CALL OTHER GUI CHANGES
                    resolve(resObj)
                    return
                }
                //alert(ele('warn'))
                reject(false)
            } catch (err) {
                console.log(err)
                reject(err)
            }
        })
        objParent.innerHTML = '';
        objParent.appendChild(
            new Element('span', `Successfully accepted chat request from ${byName}.`, ['style', 'color: #297630;']).node
        )
        setTimeout(()=>{
            objParent.setAttribute('class', objParent.getAttribute('class') + ' fade-out');
            setTimeout(()=>{
                objParent.outerHTML = '';
            }, 500)
        }, 3000)
    } catch(err){
        if(!document.getElementById('chatreq-rej-err')){
            mainRec.appendChild(new Element('p', 'Something went wrong.', ['id', 'chatreq-rej-err', 'class', 'fade-in']).node);
            setTimeout(() => {
                document.getElementById('chatreq-rej-err').setAttribute('class', 'fade-out');
                setTimeout(() => {
                    document.getElementById('chatreq-rej-err').outerHTML = '';
                }, 500)
            }, 5000);
        }
    }
}
async function chatReqReject(obj){
    const objParent = obj.parentNode;
    const byId = objParent.getAttribute('data-userid');
    const byName = objParent.getElementsByTagName('span')[0].innerText;
    const mainRec = document.getElementById('chatreq-received-main');
    //console.log(objParent);
    try{
        await new Promise(async (resolve, reject) => {
            const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
            try{
                const res = await fetch(`/api/friends/req/reject/${byId}`, {
                    method: 'post',
                    headers:{
                        'x-api-key' : apiKey
                    }
                })
                if(res.status == 200){
                    let resObj = await res
                    //CALL OTHER GUI CHANGES
                    resolve(resObj)
                    return
                }
                //alert(ele('warn'))
                reject(false)
            } catch (err) {
                console.log(err)
                reject(err)
            }
        })
        objParent.innerHTML = '';
        objParent.appendChild(
            new Element('span', `Rejected chat request from ${byName}.`, ['style', 'color: red;']).node
        )
        setTimeout(()=>{
            objParent.setAttribute('class', objParent.getAttribute('class') + ' fade-out');
            setTimeout(()=>{
                objParent.outerHTML = '';
            }, 500)
        }, 3000)
    } catch(err){
        if(!document.getElementById('chatreq-rej-err')){
            mainRec.appendChild(new Element('p', 'Something went wrong.', ['id', 'chatreq-rej-err', 'class', 'fade-in']).node);
            setTimeout(() => {
                document.getElementById('chatreq-rej-err').setAttribute('class', 'fade-out');
                setTimeout(() => {
                    document.getElementById('chatreq-rej-err').outerHTML = '';
                }, 500)
            }, 5000);
        }
    }
}
function getFetchReq(apiPath){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch(apiPath, {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                resolve(resObj)
                return
            }
            throw('failed 200')
        } catch (err) {
            reject(err)
        }
    })
}
//lock_open
function toggleLogInMenu(menuType){
    var loginContainer = document.getElementById('login-container');
    if(!loginContainer){
        if(menuType == 'login'){
           loginContainerGenLogin(loginContainer)
        } else if (menuType == 'signup'){
            loginContainerGenSignUp(loginContainer)
        }
    } else {
        if(menuType == 'login' && ele('signup-password')){
            hideLogInContainer(loginContainer, () => {
                loginContainerGenLogin(loginContainer)
            })
        } else if (menuType == 'signup' && ele('login-password')) {
            hideLogInContainer(loginContainer, () => {
                loginContainerGenSignUp(loginContainer)
            })
        } else {
            hideLogInContainer(loginContainer)
        }
    }
}

function hideLogInContainer(loginContainer, callback){
    loginContainer.childNodes.forEach((node)=>{
        node.setAttribute('class', node.getAttribute('class')+' fade-out')
    })
    setTimeout(()=>{
        loginContainer.setAttribute('class', 'login-container z-depth-2 slide-out-left');
        setTimeout(() => {
            loginContainer.outerHTML = ''
            if(callback){
                callback()
            }
        }, 490);
    }, 500)
}

function loginContainerGenLogin(loginContainer){
    loginContainer = new Element('div', null, ['class', 'login-container z-depth-2 slide-in-left', 'id', 'login-container'])
    root.appendChild(loginContainer.node)
    setTimeout(() => {
        const username = new Element('div', null, ['class', 'input-field fade-in', 'id', 'un1']).node;
        username.appendChild(new Element('input', null, ['type', 'text', 'id', 'login-name']).node);
        username.appendChild(new Element('label', 'Username', ['for', 'login-name']).node);
        loginContainer.node.appendChild(username)

        const password = new Element('div', null, ['class', 'input-field fade-in', 'id', 'psw1']).node;
        password.appendChild(new Element('input', null, ['type', 'password', 'id', 'login-password']).node);
        password.appendChild(new Element('label', 'Password', ['for', 'login-password']).node);
        loginContainer.node.appendChild(password)
        
        const checkbx = new Element('label', null, ['class', 'fade-in']).node
        checkbx.appendChild(new Element('input', null, ['type', 'checkbox', 'class', 'filled-in', 'id', 'login-rem']).node)
        checkbx.appendChild(new Element('span', 'Remember me', ['id', 'spnli1']).node)
        loginContainer.node.appendChild(checkbx)

        const btndiv = new Element('div', null, ['style', 'width:100%; text-align: left; margin-top: 10px;']).node
        const btn = new Element('button', 'Log In', ['class', 'btn fade-in light-blue darken-1 waves-effect waves-light', 'onclick', 'authLogIn()']).node
        btndiv.appendChild(btn)
        loginContainer.node.appendChild(btndiv)
    }, 500)
}

function loginContainerGenSignUp(loginContainer){
    loginContainer = new Element('div', null, ['class', 'signup-countainer login-container z-depth-2 slide-in-left', 'id', 'login-container'])
    root.appendChild(loginContainer.node)
    setTimeout(() => {
        const username = new Element('div', null, ['class', 'input-field fade-in', 'id', 'un1']).node;
        username.appendChild(new Element('input', null, ['type', 'text', 'id', 'signup-name']).node);
        username.appendChild(new Element('label', 'Username', ['for', 'signup-name']).node);
        loginContainer.node.appendChild(username)
       
        const password = new Element('div', null, ['class', 'input-field fade-in', 'id', 'psw1']).node;
        password.appendChild(new Element('input', null, ['type', 'password', 'id', 'signup-password']).node);
        password.appendChild(new Element('label', 'Password', ['for', 'signup-password']).node);
        loginContainer.node.appendChild(password)
        
        const gender = new Element('div', null, ['class', 'input-field fade-in', 'id', 'gender1'],[
            new Element('input', null, ['type', 'text', 'id', 'signup-gender']).node,
            new Element('label', 'Gender', ['for', 'signup-gender']).node
        ]).node;
        loginContainer.node.appendChild(gender);

        const country = new Element('div', null, ['class', 'input-field fade-in', 'id', 'cntry1'],[
            new Element('input', null, ['type', 'text', 'id', 'signup-country']).node,
            new Element('label', 'Country', ['for', 'signup-country']).node
        ]).node;
        loginContainer.node.appendChild(country);

        const checkbx = new Element('label', null, ['class', 'fade-in']).node
        checkbx.appendChild(new Element('input', null, ['type', 'checkbox', 'class', 'filled-in', 'id', 'signup-rem']).node)
        checkbx.appendChild(new Element('span', 'Remember me', ['id', 'spnli1']).node)
        loginContainer.node.appendChild(new Element('div', null, ['style', 'margin-top:50px;'], [checkbx]).node);

        const btndiv = new Element('div', null, ['style', 'width:100%; text-align: left; margin-top: 10px;']).node
        const btn = new Element('button', 'Sign Up', ['class', 'btn fade-in light-blue darken-1 waves-effect waves-light', 'onclick', 'signUp()']).node
        btndiv.appendChild(btn)
        loginContainer.node.appendChild(btndiv)
    }, 500)
}
async function signUp(){
    const username = document.getElementById('signup-name').value;
    const password = document.getElementById('signup-password').value;
    const gender = document.getElementById('signup-gender').value;
    const country = document.getElementById('signup-country').value;
    const remember = ele('signup-rem').checked;
    //alert(username + '\n' + password + '\n' + gender + '\n' + country + '\n' + remember);
    
    var alertMsg = null;
    if(!username) {alertMsg = 'No username.'}
    else if (/^\d/.test(username) || /\W+/.test(username)){alertMsg = 'Name must start with a letter and contain only letters and numbers.'}
    else if (username.length > 20){alertMsg = 'Name can\'t be longer than 20 characters.'}
    else if (!password) {alertMsg = 'No password.'}
    else if (password.length > 500){alertMsg = 'Password can\'t be longer than 500 characters.'}
    else if (password.length < 8){alertMsg = 'Password can\'t be shorter than 8 characters.'}
    else if (!gender) {alertMsg = 'No gender.'}
    else if (gender != 'male' && gender != 'female') {alertMsg = 'Gender can only be male or female.'}
    else if (!country) {alertMsg = 'No country.'}
    else if (/\W+/.test(country)){alertMsg = 'Country can contain only words and numbers.'}
    else if (country.length > 20){alertMsg = 'Country name can\' be longer than 20 characters.'}

    if(alertMsg){
        showAlert(alertMsg);
        return;
    }

    const nameAvailable = await getIsNameAvailable(username);
    if(!nameAvailable.available){
        showAlert('Name already taken.');
        return;
    }

    const jwt = await registerAccount(username, password, gender, country);
    if(remember){
        localStorage.setItem('x-api-key', jwt);
    } else {
        sessionStorage.setItem('x-api-key', jwt);
    }
    
    hideLogInContainer(ele('login-container'))
    setNavState(true, username)
    friendsArr = await getFriends()
    convArr = await getConvs()
    setBodyState(true, friendsArr, convArr)
    //console.log(nameAvailable)
}
function showAlert(alertText){
    if(!ele('warn')){
        let warnStyle = 'margin-top: -125px; color: red;'
        const warn = new Element('p', alertText, ['id', 'warn', 'class', 'fade-in', 'style', warnStyle]).node;
        ele('login-container').appendChild(warn)
        setTimeout(()=>{
            warn.setAttribute('class', 'fade-out');
            setTimeout(()=>{
                warn.outerHTML = ''
            }, 490)
        }, 5000)
    } else {
        ele('warn').innerText = alertText;
    }
}
async function authLogIn(){
    const username = ele('login-name').value;
    const password = ele('login-password').value;
    const remember = ele('login-rem').checked;
    //ADD ERROR HANDLING
    try{
        const res = await fetch('/api/users/login', {
            method: 'post',
            headers:{
                'content-type':'application/json; charset=utf-8'
            },
            body: JSON.stringify({
                name: username,
                password: password
            })
        })
        if(res.status == 200){
            if(remember){
                localStorage.setItem('x-api-key', res.headers.get('x-api-key'))
            } else {
                sessionStorage.setItem('x-api-key', res.headers.get('x-api-key'))
            }
            hideLogInContainer(ele('login-container'))
            setNavState(true, username)
            friendsArr = await getFriends()
            convArr = await getConvs()
            setBodyState(true, friendsArr, convArr)
            //INDICATE SUCCESSFULL LOGIN
            //CALL OTHER GUI CHANGES
            return
        }
        //alert(ele('warn'))
        if(!ele('warn')){
            let warnStyle = 'margin-top: -95px; color: red;'
            const warn = new Element('p', 'wrong username or password', ['id', 'warn', 'class', 'fade-in', 'style', warnStyle]).node;
            ele('login-container').appendChild(warn)
            setTimeout(()=>{
                warn.setAttribute('class', 'fade-out');
                setTimeout(()=>{
                    warn.outerHTML = ''
                }, 490)
            }, 5000)
        }
    } catch (err) {
        alert('err')
    }
    
}

async function authLoad(){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch('/api/users/auth', {
                method: 'post',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                username = resObj.name
                setNavState(true, username)
                //CALL OTHER GUI CHANGES
                resolve()
                return
            }
            //alert(ele('warn'))
            setNavState(false)
            resolve()
        } catch (err) {
            //console.log(err)
            reject()
        }
    })
}

function setBodyState(auth, userObjArr, convObjArr){
    //if auth is false, send a home page, otherwise send a chat window
    if(!auth){
        //build default body
        return
    }
    //build chat

    const usrName = document.querySelector('#nav-mobile')
    console.log(usrName)
    const bodyDiv = document.querySelector('.body-div');
    const chatDiv = new Element('div', null, ['class', 'chat-div  z-depth-2']).node

    const chtSrch = new Element('div', null, ['class', 'chat-srch z-depth-2']).node
    chtSrch.appendChild(new Element('input', null, ['type', 'text']).node)
    chtSrch.appendChild(new Element('i', 'search', ['class', 'material-icons', 'style', 'float:right;']).node)
    if(!userObjArr || !convObjArr){
        const viewDiv = new Element('div', null, ['class', 'view-chat-div']).node;
        viewDiv.appendChild(new Element('h1', 'It appears you have no active chats. You can add one by going to the Chat Requests section in the user menu.').node);
        chatDiv.appendChild(chtSrch);
        bodyDiv.appendChild(chatDiv)
        bodyDiv.appendChild(viewDiv)
        root.appendChild(bodyDiv)
        return;
    }
    chatDiv.appendChild(chtSrch);
    //console.log(Array(convObjArr))
    userObjArr.forEach((usrObj)=>{
        //only do the latter if there is a conversation in which ONLY both of these people are participants
        convObjArr.forEach((convObj)=>{
            
            if (convObj.participants.length == 2 && convObj.participants.find((obj)=>{
                if(obj.name == usrObj.name) return true;
            }) && convObj.participants.find((obj)=>{
                //console.log(meta.getAttributeNames())
                if(obj.name == meta.getAttribute('data-username')) return true;
            })
            ){
                const chatEntry = new Element('div', usrObj.name, ['class', 'chat-entry', 'onclick', 'changeActiveChat(this)']).node
                chatEntry.setAttribute('data-convid', convObj._id);
                chatEntry.setAttribute('data-userid', usrObj.id);
                chatEntry.setAttribute('data-lastindex', convObj.lastIndex);
                chatDiv.appendChild(chatEntry)
            }
        })
    })
    const viewDiv = new Element('div', null, ['class', 'view-chat-div']).node
    viewDiv.appendChild(new Element('h1', 'Please select a chat.').node)

    bodyDiv.appendChild(chatDiv)
    bodyDiv.appendChild(viewDiv)
    root.appendChild(bodyDiv)
    longPollMsg();
}
async function changeActiveChat(obj){
    const viewDiv = document.querySelector('div.view-chat-div');
    var alreadyActive = document.querySelector('.chat-entry.chat-entry-active');
    if(obj == alreadyActive) return;
    if(alreadyActive){
        alreadyActive.setAttribute('class', 'chat-entry');
    }
    obj.setAttribute('class', 'chat-entry chat-entry-active');
    viewDiv.innerHTML = '';

    viewDiv.appendChild(new Element('div', null, ['style', 'margin-top:50px; width:80%; text-align: center; position:absolute; color:white; z-index:2;', 'class', 'fade-in grey darken-3'], 
        [new Element('h3', obj.innerHTML, ['style', 'margin:0px; padding-bottom:6px; font-size:44px;']).node]
    ).node)

    viewDiv.appendChild(new Element('div', null, ['style', 'position: absolute; bottom: 0; width:80%; height:140px; z-index:2;', 'class', 'grey darken-3'],[
        new Element('textarea', null, ['id','msg-text' ,'style', 'border: 2px solid #fff; border-radius:20px; background-color:white; margin-top:10px; margin-left:10px; width:85%; height:120px; padding:5px; font-size:20px;',
            'class', 'browser-default']).node,
        new Element('button', 'SEND', ['class', 'send-btn light-blue darken-1', 'onclick', 'sendMessage()']).node
    ]).node)

    const convMsgs = await getConvMsgs(obj.getAttribute('data-convid'))
    const msgsDiv = new Element('div', null, ['id', 'msg-main','style', 'position:absolute; height:100%; width:80%; top:0; padding-bottom: 140px; padding-top: 104px; overflow-x:scroll;']).node
    //console.log(convMsgs)
    convMsgs.forEach((obj)=>{
        var receivedTime = new Date(Number(obj.recT))
        var strTime = `${receivedTime.getFullYear()}-${receivedTime.getMonth() + 1}-${receivedTime.getDate()} ${receivedTime.getHours()}:${receivedTime.getMinutes()}:${receivedTime.getSeconds()}`
        //console.log(receivedTime)
        if (obj.sender == meta.getAttribute('data-username')){
            msgsDiv.appendChild(new Element('div', null, ['style', 'width:100%; display:block; float:left;'],[
                new Element('div', null, ['class', 'chat-box-me z-depth-2'], [
                    new Element('p', obj.msg).node,
                    new Element('p', strTime, ['class', 'chat-entry-time']).node
                ]).node
            ]).node)
        } else {
            msgsDiv.appendChild(new Element('div', null, ['style', 'width:100%;  display:block; float:left;'],[
                new Element('div', null, ['class', 'chat-box-other z-depth-2'],[
                    new Element('p', obj.msg).node,
                    new Element('p', strTime, ['class', 'chat-entry-time']).node
                ]).node
            ]).node)
        }
    })
    viewDiv.appendChild(msgsDiv);
    msgsDiv.scrollTop = msgsDiv.scrollHeight;
}
async function sendMessage(){
    const convid = document.querySelector('.chat-entry-active').getAttribute('data-convid');
    const myMessage = document.querySelector('#msg-text').value;
    const msgMainDiv = document.querySelector('#msg-main');
    const unqT = new Date().getTime();
    //alert(convid)
    try{
        document.querySelector('#msg-text').value = ''
        msgMainDiv.appendChild(
            new Element('div', null, ['style', 'width:100%; display:block; float:left;'],[
                new Element('div', null, ['class', 'chat-box-me z-depth-2'], [
                    new Element('p', myMessage).node,
                    new Element('p', 'sending...', ['id', unqT, 'class', 'chat-entry-time'
                ]).node
            ]).node
        ]).node)
        msgMainDiv.scrollTop = msgMainDiv.scrollHeight;
        const resMsg = await new Promise(async (resolve, reject)=>{
            const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
            try{
                const res = await fetch(`/api/msgs/byid/${convid}`, {
                    method: 'post',
                    headers:{
                        'x-api-key' : apiKey,
                        'content-type':'application/json; charset=utf-8'
                    },
                    body:JSON.stringify({
                        msg: myMessage
                    })
                })
                if(res.status == 200){
                    const resObj = await res.json()
                    resolve(resObj);
                    return;
                }
                //alert(ele('warn'))
                //setNavState(false)
                throw('failed 200')
            } catch (err) {
                //console.log(err)
                reject(err)
            }
        })
        console.log(unqT);
        var receivedTime = new Date(Number(resMsg.recT));
        ele(unqT).innerText = `${receivedTime.getFullYear()}-${receivedTime.getMonth() + 1}-${receivedTime.getDate()} ${receivedTime.getHours()}:${receivedTime.getMinutes()}:${receivedTime.getSeconds()}`
        //document.querySelector('.chat-entry-active').setAttribute('data-lastindex', Number(document.querySelector('.chat-entry-active').getAttribute('data-lastindex')) + 1);
        //resMsg.recT
    } catch(err) {
        console.log(err)
    }
}
function registerAccount(username, password, gender, country){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch('/api/users/new', {
                method: 'post',
                headers:{
                    'x-api-key' : apiKey,
                    'content-type':'application/json; charset=utf-8'
                },
                body:JSON.stringify({
                    name: username,
                    password: password,
                    gender: gender,
                    country: country
                })
            })
            if(res.status == 200){
                let resObj = await res.headers.get('x-api-key')
                //CALL OTHER GUI CHANGES
                resolve(resObj);
                return;
            }
            //alert(ele('warn'))
            //setNavState(false)
            throw('failed 200')
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}
function getConvMsgs(convid){
    //send fetchreq for conversation messages -> /api/msgs/:convid
    /* testing
        return {
            id: '5c116fe0c0401919442fb46b',
            messages: [
                {
                    sender:'Jack4',
                    msg:'hello world!',
                    recT:'1544832946995'
                },
            ]
        }
    */
   return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch(`/api/msgs/byid/${convid}`, {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                //CALL OTHER GUI CHANGES
                resolve(resObj)
                return
            }
            //alert(ele('warn'))
            //setNavState(false)
            throw('failed 200')
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}
function getIsNameAvailable(name){
    return new Promise(async (resolve, reject)=>{
        //const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch(`/api/users/availablename/${name}`, {
                method: 'get',
            })
            if(res.status == 200){
                let resObj = await res.json()
                //CALL OTHER GUI CHANGES
                resolve(resObj)
                return
            }
            //alert(ele('warn'))
            //setNavState(false)
            throw('failed 200')
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}
function getFriends(){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch('/api/friends/fr', {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                //CALL OTHER GUI CHANGES
                resolve(resObj)
                return
            }
            //alert(ele('warn'))
            //setNavState(false)
            throw('failed 200')
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}
function getMyUserData(){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch('/api/users/me', {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                //CALL OTHER GUI CHANGES
                resolve(resObj)
                return
            }
            //alert(ele('warn'))
            //setNavState(false)
            throw('failed 200')
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}
function getConvs(){
    return new Promise(async (resolve, reject)=>{
        const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')
        try{
            const res = await fetch('/api/convs/meta', {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey
                }
            })
            if(res.status == 200){
                let resObj = await res.json()
                //CALL OTHER GUI CHANGES
                resolve(resObj)
                return
            }
            //alert(ele('warn'))
            //setNavState(false)
            resolve(null)
        } catch (err) {
            //console.log(err)
            reject(err)
        }
    })
}

async function onloadFnc(){
    await authLoad()
    if(localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key')){
        friendsArr = await getFriends()
        convArr = await getConvs()
        setBodyState(true, friendsArr, convArr)
    }
    /*
    //UNIT TESTING PLACEHOLDERS
        //this must be array of conv id + participants
        
        //friends -> /api/friends/fr

        //test
        const bodyStateTestArr = [
            {id: "5c00fd728a0f5827eca810d1", name:'Jack3'},
            {id: "test1", name:'Jack1'}
        ];
        

        //convs -> /api/convs/meta
        const convArr = [{
            id: "5c116fe0c0401919442fb46b",
            participants: [
            {
                id:"5c00fd728a0f5827eca810d1",
                name:"Jack3"
            },
            {
                id:"5c010e8d2d9b1e24e0bc0bde",
                name:"Jack4"
            }]
        },
        {
            id: "5c116fe0c0401919442fb46b",
            participants: [
            {
                id:"test1",
                name:"Jack1"
            },
            {
                id:"5c010e8d2d9b1e24e0bc0bde",
                name:"Jack4"
            }]
        }
        ]
       // const convMsgs = 
        setBodyState(true, bodyStateTestArr, convArr)
    */
}