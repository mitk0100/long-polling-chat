async function longPollMsg(){
    const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key');
    const chatEnt = document.getElementsByClassName('chat-entry')
    var convArr = [];
    //console.log(chatEnt)
    for(x = 0; x < chatEnt.length; x++){
        convArr.push({
            convid: chatEnt[x].getAttribute('data-convid'), 
            lastIndex: chatEnt[x].getAttribute('data-lastindex')})
    }
    //console.log(convArr)
    const res = await fetch(`/api/msgs/longpoll/${meta.getAttribute('data-username')}`, {
        method: 'get',
        headers:{
            'x-api-key' : apiKey,
            'conv-arr' : JSON.stringify(convArr)
        }
    })
    if(res.status == 200){
        let resObj = await res.json();
        const activeConvid = document.querySelector('.chat-entry-active');
        const msgsDiv = document.querySelector('#msg-main');
        console.log(resObj);
        if(!resObj.updateByIndex){
            //do stuff
            
            //console.log(activeConvid);
            const receivedTime = new Date(Number(resObj.msgObj.recT));
            const strT = `${receivedTime.getFullYear()}-${receivedTime.getMonth() + 1}-${receivedTime.getDate()} ${receivedTime.getHours()}:${receivedTime.getMinutes()}:${receivedTime.getSeconds()}`
            if(activeConvid.getAttribute('data-convid') == resObj.convid){
                msgsDiv.appendChild(new Element('div', null, ['style', 'width:100%;  display:block; float:left;'],[
                    new Element('div', null, ['class', 'chat-box-other z-depth-2'],[
                        new Element('p', resObj.msgObj.msg).node,
                        new Element('p', strT, ['class', 'chat-entry-time']).node
                    ]).node
                ]).node)
                msgsDiv.scrollTop = msgsDiv.scrollHeight;
                activeConvid.setAttribute('data-lastindex', resObj.convLastIndex);
            } else {
                //push notification
            }
        } else {
            if(activeConvid.getAttribute('data-convid') == resObj.convid){
                const msgArr = await updateConvByIndex(resObj.convid ,resObj.fromIndex);
                msgArr.messages.forEach((obj)=>{
                    if(obj.sender == activeConvid.innerHTML){
                        var receivedTime = new Date(Number(obj.recT));
                        var strT = `${receivedTime.getFullYear()}-${receivedTime.getMonth() + 1}-${receivedTime.getDate()} ${receivedTime.getHours()}:${receivedTime.getMinutes()}:${receivedTime.getSeconds()}`
                        msgsDiv.appendChild(new Element('div', null, ['style', 'width:100%;  display:block; float:left;'],[
                            new Element('div', null, ['class', 'chat-box-other z-depth-2'],[
                                new Element('p', obj.msg).node,
                                new Element('p', strT, ['class', 'chat-entry-time']).node
                            ]).node
                        ]).node)
                    }
                })
                activeConvid.setAttribute('data-lastindex', msgArr.lastIndex);
            } else {
                //push notification
            }
        }
    }
    longPollMsg();
}
//!build an api that will return all the messages from this index forward
//only append messages sent by the other user
function updateConvByIndex(convid, fromIndex){
    const apiKey = localStorage.getItem('x-api-key') || sessionStorage.getItem('x-api-key');
    return new Promise(async (resolve, reject)=>{
        try{
            const res = await fetch('/api/msgs/fromindex', {
                method: 'get',
                headers:{
                    'x-api-key' : apiKey,
                    'convid' : convid,
                    'fromindex' : fromIndex
                }
            })
            if(res.status == 200){
                let resObj = await res.json();
                //console.log(resObj);
                resolve(resObj);
            }
        }catch(err){
            reject(err);
            console.log(err)
        }
    })
}
//updateConvByIndex('5c116fe0c0401919442fb46b', 33)