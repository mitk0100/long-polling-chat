class Element {
    constructor(type, innerText, attribs, newChild, callback){
        if(!type) throw('no type');
        const newEle = document.createElement(type);
        if(attribs && attribs.length % 2 == 0){
            for(var x = 0; x < attribs.length; x += 2){
                newEle.setAttribute(attribs[x], attribs[x+1]);
            }
        } else if (attribs && attribs.length % 2 != 0) {
            throw('attributes set wrong');
        }
        if(innerText){
            newEle.innerText = innerText;
        }
        if(newChild){ 
            newChild.forEach((obj)=>{
                newEle.appendChild(obj)
            })
        }
        if(callback){
            callback(newEle)
        }
        this.node = newEle
    }
}
const ele = (id) => {
    return document.getElementById(id);
};
//more_horiz - for oprions
//menu - for menu

const root = ele('root');