const config = require('./config.js');
const users = require('./routes/users.js')
const db = require('./db.js');

(async function(){
    try{
        await db.connect(config.dbPath);
        console.log('connected to mongoDB: ', config.dbPath);
        /*
        //testing if db works ok
        (async function (){
            
            let res = await db.create('test', {name:'nasko'})
            console.log(res)
            
            newObj = {
                name2: 'nike'
            }
            
            let upd = await db.update('test', '5bff56efd034c01c0809b070', {name2:'ayyy'})
            console.log(upd)
            
            let fnd = await db.read('test', {_id: '5bff56efd034c01c0809b070'})
            console.log(fnd)
            
            let del = await db.del('test', '5bff56efd034c01c0809b070')
            console.log(del)
        })()
        */
        
        /*
        //testing user addition POST -> api/users/new
        (async function(){
            try{
                var dummy = {
                    body:{
                        name:'Jack3',
                        password:'12345678',
                        gender:'male',
                        country:'kekistan'
                    }
                }
                users.validateReqData(dummy)
                var unique = await users.uniqueName(dummy.body.name)
                if(!unique) throw('name already taken')
                var userObj = users.createUserObject(dummy)
                var dbObj = await db.create('test',userObj)
                var token = users.genToken(dbObj)
                console.log({jwt: token})
            } catch (err) {
                console.log({error : err})
            }
        })()
        */

        /*
        //testing user login POST -> api/users/login
        (async function(){
            try{
                var dummy = {
                    body:{
                        name:'Jack3',
                        password:'12345678',
                    }
                }
                var dbObj = await db.read('test',{name: dummy.body.name})
                if(!users.correctPassword(dummy.body.password, dbObj.password)){
                    throw('Wrong Password')
                }
                var token = users.genToken(dbObj)
                console.log({jwt: token})
            } catch(err){
                console.log(err)
            }
        })()
        */

        /*
        //testing user authentication POST -> api/users/auth
        (async function(){
            try{
                dummy = {
                    body: {
                        jwt: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSmFjazMiLCJfaWQiOiI1YzAwZmQ3MjhhMGY1ODI3ZWNhODEwZDEiLCJnZW5kZXIiOiJtYWxlIiwiY291bnRyeSI6Imtla2lzdGFuIiwiaWF0IjoxNTQzNTY5NzcwfQ.nNPs4bK2BCthLu4fik5IhW9F8IwxisG40NNT7Ajta1M'
                    }
                }
                //dummy.body.jwt='eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.iyJuYW1lIjoiSmFjazMiLCJfaWQiOiI1YzAwZmQ3MjhhMGY1ODI3ZWNhODEwZDEiLCJnZW5kZXIiOiJtYWxlIiwiY291bnRyeSI6Imtla2lzdGFuIiwiaWF0IjoxNTQzNTY5NzcwfQ.nNPs4bK2BCthLu4fik5IhW9F8IwxisG40NNT7Ajta1M'
                const dbObj = await users.verifyToken(dummy.body.jwt)
                console.log(dbObj)
            } catch(err){
                console.log('authentication failed')
            }   
        })()
        */
    } catch(err) {
        console.log(err)
    }
})()
