const config = require('./config.js');
const users = require('./routes/users.js');
const msgs = require('./routes/msgs.js');
///*
//testing config
console.log('+testing config');
console.log('+-PORT -', config.PORT);
console.log('+-jwtPrivateKey -', config.jwtPrivateKey);
console.log('+-dbPath -', config.dbPath);
//testing users
console.log(' ');
(function(){
    var dummy = {
        body:{
            name:'John',
            password:'12345678',
            gender:'male',
            country:'kekistan'
        }
    }
    console.log('+testing users')
    console.log('+-creating user')
    console.log(users.createUserObject(dummy))
    console.log(' ');
    console.log('+-validating user - must return true')
    console.log(users.validateReqData(dummy))
    console.log(' ');
    console.log('+-validate erroneus data - must return an error message');
    //dummy.body.gender = 'asd'
    //dummy.body.country = ''
    //dummy.body.name='1asd'
    dummy.body.name = 'jphn 1'
    try{
        console.log(users.validateReqData(dummy))
    } catch (err) {
        console.log(err)
    }
})();
//*/
/*
// testing testBlock function
(function(){
    var dummy1 = {
        _id: 'abc',
        name:'John',
        blocked: ['test', 'abd']
    }
    var dummy2 = {
        _id: 'abd',
        name:'John',
        blocked: ['test']
    }
    console.log(users.testBlock(dummy1, dummy2))
    
})()
*/

//testing msgs.createMsgObj
(function(){
    var req = {
        body: {
            msg:'a test msg <script>;"' + "'"
        }
    };
    console.log(msgs.createMsgObj(req, 'TestID'));
})()
