const srvEnv = process.env.srvEnv
var PORT;
var jwtPrivateKey;
var dbPath;

if (srvEnv == 'deployed') {
    PORT = process.env.PORT
    jwtPrivateKey = process.env.jwtPrivateKey
    dbPath = process.env.dbPath
} else {
    PORT = 3000
    jwtPrivateKey = 'testKey'
    dbPath = 'mongodb://localhost/chatApp'
}

module.exports.PORT = PORT
module.exports.jwtPrivateKey = jwtPrivateKey
module.exports.dbPath = dbPath