const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

const connect = function(path){
    return new Promise(async (resolve, reject)=>{
        try{
            const client = await MongoClient.connect(path);
            const db = client.db(client.s.options.dbName);
            functionBuilder(db)
            resolve(client)
        } catch(err){
            reject(err)
        }
    })
}

functionBuilder = function(db){
    const create = function(collection, object){
        return new Promise(async (resolve, reject) => {
            try{
                let res = await db.collection(collection).insertOne(object)
                resolve(res.ops[0])
            } catch (err) {
                reject(err)
            }
            
        })
    }
    const read = function(collection, params, projection, getAll){
        return new Promise (async (resolve, reject) => {
            try{
                if(params._id){
                    params._id = new mongodb.ObjectID(params._id)
                }
                var res
                if(!projection){
                    res = await db.collection(collection).find(params).toArray()
                } else {
                    res = await db.collection(collection).find(params).project(projection).toArray()
                }
                if(!res || res.length < 1) throw('No items found.')
                if(!getAll){
                    resolve(res[0])
                } else {
                    resolve(res)
                }
            } catch (err) {
                reject(err)
            }
        })
    }
    const update = function(collection, id, action){
        return new Promise(async (resolve, reject) => {
            try{
                //example action {$set: {name: 'Jack'}}
                id = new mongodb.ObjectID(id)
                let res = await db.collection(collection).updateOne({_id: id}, action)
                if (!res.result.n) throw('No changes were made')
                resolve(true)
            } catch (err) {
                reject(err)
            }
        })
    }
    const del = function(collection, id){
        return new Promise( async (resolve, reject) => {
            try{
                id = new mongodb.ObjectID(id)
                let res = await db.collection(collection).deleteOne({_id: id})
                if (!res.result.n) throw('Nothing was deleted')
                resolve(true)
            } catch (err) {
                reject(err)
            }
        })
    }
    exports.create = create;
    exports.read = read;
    exports.update = update;
    exports.del = del;
}

exports.mongodb = mongodb;
exports.connect = connect;